const gulp = require('gulp'),
      pug = require('gulp-pug'),
      sass = require('gulp-sass'),
      svgmin = require('gulp-svgmin'),
      rename = require("gulp-rename"),
      minify = require('gulp-clean-css'),
      strip = require('gulp-strip-comments'),
      autoprefixer = require('gulp-autoprefixer');

function swallowError (error) {
  console.log(error.toString());

  this.emit('end')
}

gulp.task('html', function(){
  return gulp.src('src/index.pug')
      .pipe(pug({
        pretty: true,
        data: {
          title: 'Hello World!'
        }
      }))
      .on('error', swallowError)
      .pipe(strip())
      .pipe(gulp.dest('dist/'))
});

gulp.task('html:watch', function () {
  return gulp.watch('src/**/*.{pug,html}', ['html']);
});

gulp.task('sass', function () {
  return gulp.src('src/style/base.scss')
      .pipe(sass()).on('error', sass.logError)
      .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
      }))
      .pipe(minify())
      .pipe(rename("style.min.css"))
      .pipe(gulp.dest('./dist/assets/css'));
});

gulp.task('sass:watch', function () {
  return gulp.watch('src/style/**/*.scss', ['sass']);
});

gulp.task('svg', function () {
  return gulp.src('src/assets/svg/*')
      .pipe(svgmin())
      .pipe(gulp.dest('./dist/assets/svg'));
});